﻿namespace Errors
{
    partial class RatingForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.client_rat_dgv = new System.Windows.Forms.DataGridView();
            this.Rating_dataGridView = new System.Windows.Forms.DataGridView();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.type_equipment_dgv = new System.Windows.Forms.DataGridView();
            this.worker_rating_dgv = new System.Windows.Forms.DataGridView();
            this.UpdateDate = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.CANCEL_BUTTON = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.client_rat_dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rating_dataGridView)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.type_equipment_dgv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.worker_rating_dgv)).BeginInit();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // client_rat_dgv
            // 
            this.client_rat_dgv.AllowUserToOrderColumns = true;
            this.client_rat_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.client_rat_dgv.Location = new System.Drawing.Point(400, 12);
            this.client_rat_dgv.Name = "client_rat_dgv";
            this.client_rat_dgv.Size = new System.Drawing.Size(385, 163);
            this.client_rat_dgv.TabIndex = 12;
            // 
            // Rating_dataGridView
            // 
            this.Rating_dataGridView.AllowUserToOrderColumns = true;
            this.Rating_dataGridView.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.Rating_dataGridView.Location = new System.Drawing.Point(12, 12);
            this.Rating_dataGridView.Name = "Rating_dataGridView";
            this.Rating_dataGridView.Size = new System.Drawing.Size(382, 163);
            this.Rating_dataGridView.TabIndex = 11;
            this.Rating_dataGridView.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Rating_dataGridView_CellContentClick);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(197, 39);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(23, 15);
            this.label2.TabIndex = 9;
            this.label2.Text = "До";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(69, 39);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 15);
            this.label1.TabIndex = 8;
            this.label1.Text = "От ";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(145, 57);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(115, 21);
            this.textBox2.TabIndex = 6;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(15, 57);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(115, 21);
            this.textBox1.TabIndex = 7;
            // 
            // type_equipment_dgv
            // 
            this.type_equipment_dgv.AllowUserToOrderColumns = true;
            this.type_equipment_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.type_equipment_dgv.Location = new System.Drawing.Point(12, 181);
            this.type_equipment_dgv.Name = "type_equipment_dgv";
            this.type_equipment_dgv.Size = new System.Drawing.Size(382, 163);
            this.type_equipment_dgv.TabIndex = 13;
            // 
            // worker_rating_dgv
            // 
            this.worker_rating_dgv.AllowUserToOrderColumns = true;
            this.worker_rating_dgv.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.worker_rating_dgv.Location = new System.Drawing.Point(400, 181);
            this.worker_rating_dgv.Name = "worker_rating_dgv";
            this.worker_rating_dgv.Size = new System.Drawing.Size(385, 163);
            this.worker_rating_dgv.TabIndex = 14;
            // 
            // UpdateDate
            // 
            this.UpdateDate.Location = new System.Drawing.Point(72, 88);
            this.UpdateDate.Name = "UpdateDate";
            this.UpdateDate.Size = new System.Drawing.Size(110, 30);
            this.UpdateDate.TabIndex = 16;
            this.UpdateDate.Text = "Обновить";
            this.UpdateDate.UseVisualStyleBackColor = true;
            this.UpdateDate.Click += new System.EventHandler(this.UpdateDate_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.textBox2);
            this.groupBox1.Controls.Add(this.UpdateDate);
            this.groupBox1.Controls.Add(this.textBox1);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.groupBox1.Location = new System.Drawing.Point(268, 350);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(279, 125);
            this.groupBox1.TabIndex = 17;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Период (американская система счисления MM-dd-YYYY)";
            // 
            // CANCEL_BUTTON
            // 
            this.CANCEL_BUTTON.Location = new System.Drawing.Point(716, 448);
            this.CANCEL_BUTTON.Name = "CANCEL_BUTTON";
            this.CANCEL_BUTTON.Size = new System.Drawing.Size(90, 27);
            this.CANCEL_BUTTON.TabIndex = 18;
            this.CANCEL_BUTTON.Text = "Выход";
            this.CANCEL_BUTTON.UseVisualStyleBackColor = true;
            this.CANCEL_BUTTON.Click += new System.EventHandler(this.CANCEL_BUTTON_Click);
            // 
            // RatingForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(808, 487);
            this.Controls.Add(this.CANCEL_BUTTON);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.worker_rating_dgv);
            this.Controls.Add(this.type_equipment_dgv);
            this.Controls.Add(this.client_rat_dgv);
            this.Controls.Add(this.Rating_dataGridView);
            this.Name = "RatingForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Мониторинг качества обслуживания:";
            this.Load += new System.EventHandler(this.RatingForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.client_rat_dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Rating_dataGridView)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.type_equipment_dgv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.worker_rating_dgv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.DataGridView client_rat_dgv;
        private System.Windows.Forms.DataGridView Rating_dataGridView;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.DataGridView type_equipment_dgv;
        private System.Windows.Forms.DataGridView worker_rating_dgv;
        private System.Windows.Forms.Button UpdateDate;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Button CANCEL_BUTTON;
    }
}