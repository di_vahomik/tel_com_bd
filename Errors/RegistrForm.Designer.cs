﻿namespace Errors
{
    partial class RegistrForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.p_Summary = new System.Windows.Forms.TextBox();
            this.label3 = new System.Windows.Forms.Label();
            this.p_Description = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.p_Customer_Address = new System.Windows.Forms.TextBox();
            this.p_Customer_Phone = new System.Windows.Forms.TextBox();
            this.p_Customer_Company = new System.Windows.Forms.TextBox();
            this.p_Customer_Contact = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.btn_addProblem = new System.Windows.Forms.Button();
            this.dg_registration = new System.Windows.Forms.DataGridView();
            this.CANCEL_BUTTON = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dg_registration)).BeginInit();
            this.SuspendLayout();
            // 
            // p_Summary
            // 
            this.p_Summary.Location = new System.Drawing.Point(14, 348);
            this.p_Summary.Multiline = true;
            this.p_Summary.Name = "p_Summary";
            this.p_Summary.ReadOnly = true;
            this.p_Summary.Size = new System.Drawing.Size(404, 72);
            this.p_Summary.TabIndex = 53;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(11, 332);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 52;
            this.label3.Text = "Резюме";
            // 
            // p_Description
            // 
            this.p_Description.Location = new System.Drawing.Point(14, 252);
            this.p_Description.Multiline = true;
            this.p_Description.Name = "p_Description";
            this.p_Description.ReadOnly = true;
            this.p_Description.Size = new System.Drawing.Size(404, 72);
            this.p_Description.TabIndex = 51;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(11, 236);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 50;
            this.label2.Text = "Описание проблемы";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(447, 340);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(91, 13);
            this.label11.TabIndex = 49;
            this.label11.Text = "Адрес компании";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(447, 314);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(91, 13);
            this.label10.TabIndex = 48;
            this.label10.Text = "Контактный тел.";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(447, 288);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(93, 13);
            this.label9.TabIndex = 47;
            this.label9.Text = "Контактное лицо";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(447, 262);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(58, 13);
            this.label8.TabIndex = 46;
            this.label8.Text = "Компания";
            // 
            // p_Customer_Address
            // 
            this.p_Customer_Address.Location = new System.Drawing.Point(553, 337);
            this.p_Customer_Address.Name = "p_Customer_Address";
            this.p_Customer_Address.ReadOnly = true;
            this.p_Customer_Address.Size = new System.Drawing.Size(236, 20);
            this.p_Customer_Address.TabIndex = 45;
            // 
            // p_Customer_Phone
            // 
            this.p_Customer_Phone.Location = new System.Drawing.Point(553, 311);
            this.p_Customer_Phone.Name = "p_Customer_Phone";
            this.p_Customer_Phone.ReadOnly = true;
            this.p_Customer_Phone.Size = new System.Drawing.Size(236, 20);
            this.p_Customer_Phone.TabIndex = 44;
            // 
            // p_Customer_Company
            // 
            this.p_Customer_Company.Location = new System.Drawing.Point(553, 259);
            this.p_Customer_Company.Name = "p_Customer_Company";
            this.p_Customer_Company.ReadOnly = true;
            this.p_Customer_Company.Size = new System.Drawing.Size(236, 20);
            this.p_Customer_Company.TabIndex = 43;
            // 
            // p_Customer_Contact
            // 
            this.p_Customer_Contact.Location = new System.Drawing.Point(553, 285);
            this.p_Customer_Contact.Name = "p_Customer_Contact";
            this.p_Customer_Contact.ReadOnly = true;
            this.p_Customer_Contact.Size = new System.Drawing.Size(236, 20);
            this.p_Customer_Contact.TabIndex = 42;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(435, 236);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(126, 13);
            this.label6.TabIndex = 41;
            this.label6.Text = "Информация о клиенте";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(11, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(171, 13);
            this.label1.TabIndex = 40;
            this.label1.Text = "Зарегистрированные проблемы";
            // 
            // btn_addProblem
            // 
            this.btn_addProblem.Location = new System.Drawing.Point(438, 415);
            this.btn_addProblem.Name = "btn_addProblem";
            this.btn_addProblem.Size = new System.Drawing.Size(252, 23);
            this.btn_addProblem.TabIndex = 39;
            this.btn_addProblem.Text = "Зарегистрировать новую проблему";
            this.btn_addProblem.UseVisualStyleBackColor = true;
            this.btn_addProblem.Click += new System.EventHandler(this.btn_addProblem_Click);
            // 
            // dg_registration
            // 
            this.dg_registration.AllowUserToAddRows = false;
            this.dg_registration.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dg_registration.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_registration.Location = new System.Drawing.Point(13, 29);
            this.dg_registration.Name = "dg_registration";
            this.dg_registration.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_registration.Size = new System.Drawing.Size(776, 193);
            this.dg_registration.TabIndex = 38;
            this.dg_registration.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.Dg_registration_CellClick);
            this.dg_registration.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_registration_CellContentClick);
            // 
            // CANCEL_BUTTON
            // 
            this.CANCEL_BUTTON.Location = new System.Drawing.Point(705, 416);
            this.CANCEL_BUTTON.Name = "CANCEL_BUTTON";
            this.CANCEL_BUTTON.Size = new System.Drawing.Size(83, 21);
            this.CANCEL_BUTTON.TabIndex = 54;
            this.CANCEL_BUTTON.Text = "Выход";
            this.CANCEL_BUTTON.UseVisualStyleBackColor = true;
            this.CANCEL_BUTTON.Click += new System.EventHandler(this.CANCEL_BUTTON_Click);
            // 
            // RegistrForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Snow;
            this.ClientSize = new System.Drawing.Size(809, 450);
            this.Controls.Add(this.CANCEL_BUTTON);
            this.Controls.Add(this.p_Summary);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.p_Description);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.p_Customer_Address);
            this.Controls.Add(this.p_Customer_Phone);
            this.Controls.Add(this.p_Customer_Company);
            this.Controls.Add(this.p_Customer_Contact);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.btn_addProblem);
            this.Controls.Add(this.dg_registration);
            this.Name = "RegistrForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Регистрация";
            this.Load += new System.EventHandler(this.RegistrForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dg_registration)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.TextBox p_Summary;
        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox p_Description;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox p_Customer_Address;
        public System.Windows.Forms.TextBox p_Customer_Phone;
        public System.Windows.Forms.TextBox p_Customer_Company;
        public System.Windows.Forms.TextBox p_Customer_Contact;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btn_addProblem;
        private System.Windows.Forms.DataGridView dg_registration;
        private System.Windows.Forms.Button CANCEL_BUTTON;
    }
}