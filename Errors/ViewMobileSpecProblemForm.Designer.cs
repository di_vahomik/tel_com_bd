﻿namespace Errors
{
    partial class ViewMobileSpecProblemForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label3 = new System.Windows.Forms.Label();
            this.p_Worker_Rating = new System.Windows.Forms.TextBox();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.p_Worker_Skills = new System.Windows.Forms.TextBox();
            this.p_Worker_Patronymic = new System.Windows.Forms.TextBox();
            this.p_Worker_Surname = new System.Windows.Forms.TextBox();
            this.p_Worker_Name = new System.Windows.Forms.TextBox();
            this.label6 = new System.Windows.Forms.Label();
            this.p_Description = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.dg_profiling = new System.Windows.Forms.DataGridView();
            this.CHANGE_STATUS_OG_PROBLEM = new System.Windows.Forms.Button();
            this.CANCEL_BUTTON = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.dg_profiling)).BeginInit();
            this.SuspendLayout();
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(444, 381);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(48, 13);
            this.label3.TabIndex = 79;
            this.label3.Text = "Рейтинг";
            // 
            // p_Worker_Rating
            // 
            this.p_Worker_Rating.Location = new System.Drawing.Point(550, 378);
            this.p_Worker_Rating.Name = "p_Worker_Rating";
            this.p_Worker_Rating.ReadOnly = true;
            this.p_Worker_Rating.Size = new System.Drawing.Size(236, 20);
            this.p_Worker_Rating.TabIndex = 78;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Location = new System.Drawing.Point(444, 355);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(47, 13);
            this.label11.TabIndex = 77;
            this.label11.Text = "Навыки";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Location = new System.Drawing.Point(444, 329);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(54, 13);
            this.label10.TabIndex = 76;
            this.label10.Text = "Отчество";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Location = new System.Drawing.Point(444, 303);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(29, 13);
            this.label9.TabIndex = 75;
            this.label9.Text = "Имя";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(444, 277);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(56, 13);
            this.label8.TabIndex = 74;
            this.label8.Text = "Фамилия";
            // 
            // p_Worker_Skills
            // 
            this.p_Worker_Skills.Location = new System.Drawing.Point(550, 352);
            this.p_Worker_Skills.Name = "p_Worker_Skills";
            this.p_Worker_Skills.ReadOnly = true;
            this.p_Worker_Skills.Size = new System.Drawing.Size(236, 20);
            this.p_Worker_Skills.TabIndex = 73;
            // 
            // p_Worker_Patronymic
            // 
            this.p_Worker_Patronymic.Location = new System.Drawing.Point(550, 326);
            this.p_Worker_Patronymic.Name = "p_Worker_Patronymic";
            this.p_Worker_Patronymic.ReadOnly = true;
            this.p_Worker_Patronymic.Size = new System.Drawing.Size(236, 20);
            this.p_Worker_Patronymic.TabIndex = 72;
            // 
            // p_Worker_Surname
            // 
            this.p_Worker_Surname.Location = new System.Drawing.Point(550, 274);
            this.p_Worker_Surname.Name = "p_Worker_Surname";
            this.p_Worker_Surname.ReadOnly = true;
            this.p_Worker_Surname.Size = new System.Drawing.Size(236, 20);
            this.p_Worker_Surname.TabIndex = 71;
            // 
            // p_Worker_Name
            // 
            this.p_Worker_Name.Location = new System.Drawing.Point(550, 300);
            this.p_Worker_Name.Name = "p_Worker_Name";
            this.p_Worker_Name.ReadOnly = true;
            this.p_Worker_Name.Size = new System.Drawing.Size(236, 20);
            this.p_Worker_Name.TabIndex = 70;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Location = new System.Drawing.Point(432, 251);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(211, 13);
            this.label6.TabIndex = 69;
            this.label6.Text = "Информация о мобильном специалисте";
            // 
            // p_Description
            // 
            this.p_Description.Location = new System.Drawing.Point(12, 267);
            this.p_Description.Multiline = true;
            this.p_Description.Name = "p_Description";
            this.p_Description.ReadOnly = true;
            this.p_Description.Size = new System.Drawing.Size(404, 81);
            this.p_Description.TabIndex = 68;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(9, 251);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 13);
            this.label2.TabIndex = 67;
            this.label2.Text = "Описание проблемы";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(9, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(444, 13);
            this.label1.TabIndex = 66;
            this.label1.Text = "Просмотр назначенных для текущего специалиста запросов на устранение проблем:";
            // 
            // dg_profiling
            // 
            this.dg_profiling.AllowUserToAddRows = false;
            this.dg_profiling.BackgroundColor = System.Drawing.SystemColors.ControlLight;
            this.dg_profiling.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dg_profiling.Location = new System.Drawing.Point(12, 33);
            this.dg_profiling.Name = "dg_profiling";
            this.dg_profiling.SelectionMode = System.Windows.Forms.DataGridViewSelectionMode.FullRowSelect;
            this.dg_profiling.Size = new System.Drawing.Size(776, 203);
            this.dg_profiling.TabIndex = 65;
            this.dg_profiling.CellClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_profiling_CellALlContentClick);
            this.dg_profiling.CellContentClick += new System.Windows.Forms.DataGridViewCellEventHandler(this.dg_profiling_CellContentClick);
            // 
            // CHANGE_STATUS_OG_PROBLEM
            // 
            this.CHANGE_STATUS_OG_PROBLEM.Location = new System.Drawing.Point(485, 414);
            this.CHANGE_STATUS_OG_PROBLEM.Name = "CHANGE_STATUS_OG_PROBLEM";
            this.CHANGE_STATUS_OG_PROBLEM.Size = new System.Drawing.Size(180, 25);
            this.CHANGE_STATUS_OG_PROBLEM.TabIndex = 80;
            this.CHANGE_STATUS_OG_PROBLEM.Text = "Изменить статус проблемы";
            this.CHANGE_STATUS_OG_PROBLEM.UseVisualStyleBackColor = true;
            this.CHANGE_STATUS_OG_PROBLEM.Click += new System.EventHandler(this.CHANGE_STATUS_OG_PROBLEM_Click);
            // 
            // CANCEL_BUTTON
            // 
            this.CANCEL_BUTTON.Location = new System.Drawing.Point(702, 414);
            this.CANCEL_BUTTON.Name = "CANCEL_BUTTON";
            this.CANCEL_BUTTON.Size = new System.Drawing.Size(84, 24);
            this.CANCEL_BUTTON.TabIndex = 81;
            this.CANCEL_BUTTON.Text = "Выход";
            this.CANCEL_BUTTON.UseVisualStyleBackColor = true;
            this.CANCEL_BUTTON.Click += new System.EventHandler(this.CANCEL_BUTTON_Click);
            // 
            // ViewMobileSpecProblemForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.LightGreen;
            this.ClientSize = new System.Drawing.Size(800, 450);
            this.Controls.Add(this.CANCEL_BUTTON);
            this.Controls.Add(this.CHANGE_STATUS_OG_PROBLEM);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.p_Worker_Rating);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.p_Worker_Skills);
            this.Controls.Add(this.p_Worker_Patronymic);
            this.Controls.Add(this.p_Worker_Surname);
            this.Controls.Add(this.p_Worker_Name);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.p_Description);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.dg_profiling);
            this.Name = "ViewMobileSpecProblemForm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Просмотр назначенных для текущего мобильного специалиста запросов на устранение п" +
    "роблем:";
            this.Load += new System.EventHandler(this.ViewMobileSpecProblemForm_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dg_profiling)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label3;
        public System.Windows.Forms.TextBox p_Worker_Rating;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label8;
        public System.Windows.Forms.TextBox p_Worker_Skills;
        public System.Windows.Forms.TextBox p_Worker_Patronymic;
        public System.Windows.Forms.TextBox p_Worker_Surname;
        public System.Windows.Forms.TextBox p_Worker_Name;
        private System.Windows.Forms.Label label6;
        public System.Windows.Forms.TextBox p_Description;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.DataGridView dg_profiling;
        private System.Windows.Forms.Button CHANGE_STATUS_OG_PROBLEM;
        private System.Windows.Forms.Button CANCEL_BUTTON;
    }
}