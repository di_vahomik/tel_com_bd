﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Errors
{
    public partial class ProffilingForm : Form
    {
        private readonly string ConnectionString = Form1.ConnectionString;

        public ProffilingForm()
        {
            InitializeComponent();
        }

        private void updateData()
        {
            var request = "SELECT * FROM Problem_Table FULL OUTER JOIN Worker_Table ON Problem_Table.Worker_Id = Worker_Table.Id";
            var adapter = new SqlDataAdapter(request, ConnectionString);
            var table = new DataTable();
            adapter.Fill(table);
            dg_profiling.DataSource = table;
            dg_profiling.Columns["Id"].Visible = false;
            dg_profiling.Columns["TypeOfEquipment"].HeaderText = "Тип оборудования";
            dg_profiling.Columns["SerialNum"].HeaderText = "Серийный номер";
            dg_profiling.Columns["TypeOfProblem"].HeaderText = "Тип проблемы";
            dg_profiling.Columns["Description"].Visible = false;
            dg_profiling.Columns["Priority"].HeaderText = "Приоритет";
            dg_profiling.Columns["Customer_Source"].HeaderText = "Клиент";
            dg_profiling.Columns["Customer_Contact"].Visible = false;
            dg_profiling.Columns["Customer_Phone"].Visible = false;
            dg_profiling.Columns["Customer_Adress"].Visible = false;
            dg_profiling.Columns["DateOfRegistration"].HeaderText = "Проблема зарегистрирована";
            dg_profiling.Columns["Status"].Visible = false;
            dg_profiling.Columns["DateOfStatus"].Visible = false;
            dg_profiling.Columns["Resume"].Visible = false;
            dg_profiling.Columns["Worker_Id"].Visible = false;

            dg_profiling.Columns["Id1"].Visible = false;
            dg_profiling.Columns["Login"].Visible = false;
            dg_profiling.Columns["Password"].Visible = false;
            dg_profiling.Columns["Name"].Visible = false;
            dg_profiling.Columns["Patronymic"].Visible = false;
            dg_profiling.Columns["Skills"].Visible = false;
            dg_profiling.Columns["Score"].Visible = false;
            dg_profiling.Columns["Surname"].HeaderText = "Моб. специалист";

            dg_profiling.AutoSizeColumnsMode = DataGridViewAutoSizeColumnsMode.DisplayedCells;

        }


        private void updateWindows()
        {
            var mas = dg_profiling.SelectedRows;
            var candidat = mas[0].Cells[0].FormattedValue.ToString();

            var con = new SqlConnection(ConnectionString);
            con.Open();
            var request = "SELECT Description FROM Problem_Table WHERE Id = " + candidat + "";
            var com = new SqlCommand(request, con);
            var last = com.ExecuteScalar();
            p_Description.Text = last.ToString();

            request = "SELECT Worker_Id FROM Problem_Table WHERE Id = " + candidat + "";
            com = new SqlCommand(request, con);
            last = com.ExecuteScalar();
            string wTest = last.ToString();
            if (wTest != "")
            {
                int wID = (int)last;

                request = "SELECT Name FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                p_Worker_Name.Text = last.ToString();

                request = "SELECT Surname FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                p_Worker_Surname.Text = last.ToString();

                request = "SELECT Patronymic FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                p_Worker_Patronymic.Text = last.ToString();

                request = "SELECT Skills FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                p_Worker_Skills.Text = last.ToString();

                request = "SELECT Score FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                p_Worker_Rating.Text = last.ToString();
            }
            else
            {
                p_Worker_Name.Text = "(Не назначен)";
                p_Worker_Surname.Text = "(Не назначен)";
                p_Worker_Patronymic.Text = "(Не назначен)";
                p_Worker_Skills.Text = "(Не назначен)";
                p_Worker_Rating.Text = "(Не назначен)";
            }
            con.Close();
        }

        private void btn_addWorker_Click(object sender, EventArgs e)
        {
            var form = new ProfillingAddNewProfil();
            if (form.ShowDialog() == DialogResult.OK)
            {

                var surname = form.Surname_pnew.Text;
                var name = form.name_pnew.Text;
                var patronymic = form.Patronymic_pnew.Text;
                var login = form.Login_pnew.Text;
                var password = form.Password_pnew.Text;
                var skills = form.Skills_pnew.Text;
                var score = form.Score_pnew.Text;
                var connection = new SqlConnection(ConnectionString);
                connection.Open();
                var request = "INSERT INTO Worker_Table (Name, Surname, Patronymic, Login, Password, Skills, Score )" + "VALUES ('" + name + "', '" + surname + "', '"
                    + patronymic + "', '" + login + "', '" + password + "', '" + skills + "', '" + score + "')";
                var command = new SqlCommand(request, connection);
                command.ExecuteNonQuery();
                connection.Close();
            }
            
        }

        private void btn_linkWorker_Click(object sender, EventArgs e)
        {
            var form = new ProfillingAddProfilForm();

            var mas = dg_profiling.SelectedRows;
            var candidat = mas[0].Cells[0].FormattedValue.ToString();

            {
                var krequest = "Select Id, Surname, Name, Patronymic FROM Worker_Table";
                var adapter = new SqlDataAdapter(krequest, ConnectionString);
                var dict = new Dictionary<int, string>();
                var tbl = new DataTable();
                adapter.Fill(tbl);

                foreach (DataRow row in tbl.Rows)
                {
                    string setS = row["Surname"].ToString() + " " + row["Name"].ToString() + " " + row["Patronymic"].ToString();
                    dict.Add((int)row["Id"], setS);
                }

                form.Data = dict;
            }
           

            var con = new SqlConnection(ConnectionString);
            con.Open();
            var request = "SELECT Priority FROM Problem_Table WHERE Id = " + candidat + "";
            var com = new SqlCommand(request, con);
            var last = com.ExecuteScalar();
            form.linkP_Priority.Text = last.ToString();

            request = "SELECT Worker_id FROM Problem_Table WHERE Id = " + candidat + "";
            com = new SqlCommand(request, con);
            last = com.ExecuteScalar();
            string wTest = last.ToString();
            if (wTest != "")
            {
                int wID = (int)last;
                string strOut = "";

                request = "SELECT Surname FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                strOut += last.ToString() + " ";

                request = "SELECT Name FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                strOut += last.ToString() + " ";

                request = "SELECT Patronymic FROM Worker_Table WHERE Id = " + wID + "";
                com = new SqlCommand(request, con);
                last = com.ExecuteScalar();
                strOut += last.ToString();
                form.cb_linkWorker.Text = strOut;
            }

            con.Close();

            if (form.ShowDialog() == DialogResult.OK)
            {
                con = new SqlConnection(ConnectionString);
                con.Open();
                request = "UPDATE Problem_Table SET Worker_Id= '" + form.Id + "' WHERE Id='" + candidat + "'";
                com = new SqlCommand(request, con);
                com.ExecuteNonQuery();

                request = "UPDATE Problem_Table SET Priority= N'" + form.linkP_Priority.Text + "' WHERE Id='" + candidat + "'";
                com = new SqlCommand(request, con);
                com.ExecuteNonQuery();

                request = "UPDATE Problem_Table SET Status= N'" + "responsible person appointed" + "' WHERE Id='" + candidat + "'";
                com = new SqlCommand(request, con);
                com.ExecuteNonQuery();

                DateTime myDateTime = DateTime.Now;
                string sqlFormattedDate = myDateTime.ToString("yyyy-MM-dd HH:mm:ss.fff");

                request = "UPDATE Problem_Table SET DateOfStatus= '" + sqlFormattedDate + "' WHERE Id='" + candidat + "'";
                com = new SqlCommand(request, con);
                com.ExecuteNonQuery();
                con.Close();

                updateData();
                updateWindows();
            }
        }

        private void CANCEL_BUTTON_Click(object sender, EventArgs e)
        {
            DialogResult = DialogResult.Cancel;
            Close();
        }

        private void ProffilingForm_Load(object sender, EventArgs e)
        {
            updateData();
            updateWindows();
        }

        private void dg_profiling_CellContentClick(object sender, DataGridViewCellEventArgs e)
        {
            updateWindows();
        }
    }
}
